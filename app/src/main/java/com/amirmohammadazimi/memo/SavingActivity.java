package com.amirmohammadazimi.memo;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amirmohammadazimi.memo.model.Memory;

import java.util.Calendar;

public class SavingActivity extends AppCompatActivity {

    static int year, month, day, hourOfDay, minute;
    String title, location, description, imagePath;
    private final static int SELECT_PHOTO = 12345;

    ImageView imageView;
    static TextView dateView, timeView;
    EditText titleText, locationText, descTex;

    Intent intent;
    int id;
    Memory willBeEdited;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving);
        intent = getIntent();

        imageView = (ImageView) findViewById(R.id.imageView);
        dateView = (TextView) findViewById(R.id.date_view);
        timeView = (TextView) findViewById(R.id.time_view);

        titleText = (EditText) findViewById(R.id.title_text);
        locationText = (EditText) findViewById(R.id.location_text);
        descTex = (EditText) findViewById(R.id.description);


        id = intent.getIntExtra("id", -1);
        if (intent.getStringExtra("type").equals("edit")) {
            if (id != -1) {
                willBeEdited = MainActivity.memories.getMemory(id);

                titleText.setText(willBeEdited.getTitle());
                locationText.setText(willBeEdited.getLocation());
                descTex.setText(willBeEdited.getDescription());

                dateView.setText(willBeEdited.getCalendar().get(Calendar.YEAR) + " / " + (willBeEdited.getCalendar().get(Calendar.MONTH) + 1) + " / " + willBeEdited.getCalendar().get(Calendar.DAY_OF_MONTH));

                timeView.setText(willBeEdited.getCalendar().get(Calendar.HOUR_OF_DAY) % 12 + " : " + willBeEdited.getCalendar().get(Calendar.MINUTE) + "  " + ((willBeEdited.getCalendar().get(Calendar.HOUR_OF_DAY) < 12) ? "am" : "pm"));

                imagePath = willBeEdited.getImagePath();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
                imageView.setImageBitmap(bitmap);

                // Do something with the bitmap

                imageView.setVisibility(View.VISIBLE);
            }

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI
            Uri pickedImage = data.getData();
            // Let's read picked image path using content resolver
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
            imageView.setImageBitmap(bitmap);

            // Do something with the bitmap

            imageView.setVisibility(View.VISIBLE);
            // At the end remember to close the cursor or you will end with the RuntimeException!
            cursor.close();
        }
    }


    public void pickImage(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }


    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dateView.setText(year + " / " + (month + 1) + " / " + day);
            SavingActivity.year = year;
            SavingActivity.month = month;
            SavingActivity.day = day;
        }
    }

    public void showTimePickerDialog(View v) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.show(this.getFragmentManager(), "timePicker");
    }

    public void save(View view) {

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hourOfDay, minute);

        title = titleText.getText().toString();
        location = locationText.getText().toString();
        description = descTex.getText().toString();

        if (intent.getStringExtra("type").equals("new")) {
            MainActivity.memories.add(new Memory(title, location, description, imagePath, cal));
        } else if (intent.getStringExtra("type").equals("edit")) {
            MainActivity.memories.getMemory(id).setTitle(title);
            MainActivity.memories.getMemory(id).setLocation(location);
            MainActivity.memories.getMemory(id).setCalendar(cal);
            MainActivity.memories.getMemory(id).setDescription(description);
            MainActivity.memories.getMemory(id).setImagePath(imagePath);

        }
        Toast.makeText(this, "Saved ", Toast.LENGTH_SHORT).show();
        Log.d("In Saving", "save: " + MainActivity.memories.memos);

        finish();

    }

}
