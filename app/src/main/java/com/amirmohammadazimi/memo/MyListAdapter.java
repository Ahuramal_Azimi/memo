package com.amirmohammadazimi.memo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amirmohammadazimi.memo.model.Memory;

import java.util.ArrayList;

/**
 * Created by Amir Mohammad Azimi on 4/17/2017.
 */

public class MyListAdapter extends ArrayAdapter<Memory> {

    private final Activity context;

    Boolean listDisplayType;

    private ArrayList<Memory> originalList;
    private ArrayList<Memory> memoryArrayList;
    private MemoryFilter filter;

    public MyListAdapter(Activity context,ArrayList<Memory> memoryArrayList, Boolean listDisplayType) {
        super(context, R.layout.mylist, memoryArrayList);

        this.context = context;
        this.memoryArrayList = new ArrayList<Memory>();
        this.memoryArrayList.addAll(memoryArrayList);
        this.originalList = new ArrayList<Memory>();
        this.originalList.addAll(memoryArrayList);

        this.listDisplayType = listDisplayType;
    }

    public View getView(int position, View view, ViewGroup parent) {

        Memory tempMemo = memoryArrayList.get(position);

        if (listDisplayType) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.mylist_extended, null, true);
            }

            TextView txtTitle = (TextView) view.findViewById(R.id.list_title);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            TextView descTxt = (TextView) view.findViewById(R.id.list_desc);
            TextView localTxt = (TextView) view.findViewById(R.id.list_location);
            TextView dateTxt = (TextView) view.findViewById(R.id.list_Date);


            txtTitle.setText(tempMemo.getTitle());
            descTxt.setText(tempMemo.getDescription());
            localTxt.setText(tempMemo.getLocation());
            dateTxt.setText(tempMemo.getCalendar().getTime().toString());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(tempMemo.getImagePath(), options);
            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);

            return view;
        } else {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.mylist, null, true);
            }

            TextView txtTitle = (TextView) view.findViewById(R.id.list_title);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            TextView descTxt = (TextView) view.findViewById(R.id.list_desc);

            txtTitle.setText(tempMemo.getTitle());
            descTxt.setText(tempMemo.getDescription());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(tempMemo.getImagePath(), options);
            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);

            return view;

        }
    }



    private class MemoryFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<Memory> filteredItems = new ArrayList<Memory>();

                for(int i = 0, l = originalList.size(); i < l; i++)
                {
                    Memory memory = originalList.get(i);
                    if(memory.toString().toLowerCase().contains(constraint))
                        filteredItems.add(memory);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            }
            else
            {
                synchronized(this)
                {
                    result.values = originalList;
                    result.count = originalList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            memoryArrayList = (ArrayList<Memory>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = memoryArrayList.size(); i < l; i++)
                add(memoryArrayList.get(i));
            notifyDataSetInvalidated();
        }
    }

    @Override
    public Filter getFilter() {
        if (filter == null){
            filter  = new MemoryFilter();
        }
        return filter;
    }



}
