package com.amirmohammadazimi.memo.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Amir Mohammad Azimi on 4/16/2017.
 */

public class Memories {

    public HashMap<Integer, Memory> memos = new HashMap<Integer, Memory>();


    public void add(Memory memory) {
        memos.put(memory.getId(), memory);
    }

    public void delete(int id) {

        memos.remove(id);
        Log.d("In_Deleting", "deleted: " + id);
        Log.d("ifDeleted", memos + "");

    }


    public Memory getMemory(int id) {
        return memos.get(id);
    }

    public ArrayList<Memory> getMemories() {
        ArrayList<Memory> DATA = new ArrayList<Memory>();

        for (Memory memo : memos.values()) {
            DATA.add(memo);
        }

        return DATA;

    }
}
