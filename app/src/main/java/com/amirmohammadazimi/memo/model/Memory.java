package com.amirmohammadazimi.memo.model;

import java.util.Calendar;

/**
 * Created by Amir Mohammad Azimi on 4/16/2017.
 */

public class Memory {
    private int id;
    private static int count = 0;
    private String title, location, description, imagePath;
    private Calendar calendar;


    public Memory(String title, String location, String description, String imagePath, Calendar calendar) {
        this.id = ++count;
        this.title = title;
        this.location = location;
        this.description = description;
        this.imagePath = imagePath;
        this.calendar = calendar;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    @Override
    public String toString() {
        return title;
    }
}