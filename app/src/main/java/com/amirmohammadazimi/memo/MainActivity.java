package com.amirmohammadazimi.memo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.amirmohammadazimi.memo.model.Memories;
import com.amirmohammadazimi.memo.model.Memory;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static Memories memories;

    Button newMemo;
    MyListAdapter myListAdapter;
    Switch adapterSwitch;
    Boolean listDisplayType;

    ListView myList;
    ArrayList<Memory> memoryArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        memories = new Memories();

        listDisplayType = false;

        newMemo = (Button) findViewById(R.id.newMemo);
        newMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SavingActivity.class);
                intent.putExtra("type", "new");
                startActivity(intent);
            }
        });

        myList = (ListView) findViewById(R.id.myList);
        registerForContextMenu(myList);


        adapterSwitch = (Switch) findViewById(R.id.adapterSwitch);
        adapterSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                listDisplayType = isChecked;
                onResume();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("baa", "onResume: ");

        memoryArrayList = memories.getMemories();


        myListAdapter = new MyListAdapter(this, memoryArrayList, listDisplayType);
        myList.setAdapter(myListAdapter);

        EditText myFilter = (EditText) findViewById(R.id.searchList);
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                myListAdapter.getFilter().filter(s.toString());
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.myList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(memoryArrayList.get(info.position).getTitle());
            String[] menuItems = {"Edit", "Delete"};
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = {"Edit", "Delete"};
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = memoryArrayList.get(info.position).getTitle();

        switch (menuItemName) {
            case "Delete":
                memories.delete(memoryArrayList.get(info.position).getId());
                onResume();
                break;
            case "Edit":
                edit(memoryArrayList.get(info.position).getId());
                break;
        }
        Toast.makeText(MainActivity.this, String.format("Selected %s for item %s", menuItemName, listItemName), Toast.LENGTH_SHORT).show();
        return true;
    }

    public void edit(int id) {
        Intent intent = new Intent(MainActivity.this, SavingActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("type", "edit");
        startActivity(intent);
    }

}
