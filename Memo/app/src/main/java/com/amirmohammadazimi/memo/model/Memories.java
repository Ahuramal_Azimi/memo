package com.amirmohammadazimi.memo.model;

import android.util.Log;

import java.util.HashMap;


/**
 * Created by Amir Mohammad Azimi on 4/16/2017.
 */

public class Memories {

    public HashMap<Integer, Memory> memos = new HashMap<Integer, Memory>();

    public final int MEMO_TITLE = 1;
    public final int MEMO = 2;


    public void add(Memory memory) {
        memos.put(memory.getId(), memory);
    }

    public void delete(int id) {

        memos.remove(id);
        Log.d("In_Deleting", "deleted: " + id);
        Log.d("ifDeleted", memos + "");

    }


    public Memory getMemory(int id) {
        return memos.get(id);
    }

    public Object[] getMemories(int what) {
        Object[] DATA = null;

        int i = 0;
        int j = 0;
        switch (what) {
            case MEMO_TITLE:
                DATA = new String[memos.size()];
                for (Memory memo : memos.values()) {
                    DATA[i] = memo.getTitle();
                    ++i;
                }
                ;
                break;
            case MEMO:
                DATA = new Memory[memos.size()];
                for (Memory memo : memos.values()) {
                    DATA[j] = memo;
                    ++j;
                }
                ;
                break;
        }
        return DATA;
    }


}
