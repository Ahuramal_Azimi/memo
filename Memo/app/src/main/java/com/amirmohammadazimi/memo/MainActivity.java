package com.amirmohammadazimi.memo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.amirmohammadazimi.memo.model.Memories;
import com.amirmohammadazimi.memo.model.Memory;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static Memories memories;

    Button newMemo;
    MyListAdapter myListAdapter;
    Switch adapterSwitch;
    Boolean listDisplayType;
    int listId;

    ListView myList;
    String[] memoName;
    Memory[] memoArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        memories = new Memories();

        listDisplayType = false;

        newMemo = (Button) findViewById(R.id.newMemo);
        newMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this , SavingActivity.class);

                startActivity(intent);
            }
        });

        myList  = (ListView) findViewById(R.id.myList);
        registerForContextMenu(myList);
        doSearch();
//        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, "" + position + " , " + id, Toast.LENGTH_SHORT).show();
//                listId = position;
//                memories.delete(memoArray[position].getId());
//                onResume();
//                return false;
//            }
//        });

        adapterSwitch = (Switch) findViewById(R.id.adapterSwitch);
        adapterSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                listDisplayType = isChecked;
                onResume();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("baa", "onResume: ");

        memoName = (String[]) memories.getMemories(memories.MEMO_TITLE);
        memoArray = (Memory[]) memories.getMemories(memories.MEMO);

        myListAdapter = new MyListAdapter(this , memoName  , memoArray, listDisplayType);
        myList.setAdapter(myListAdapter);
    }

    private void doSearch() {
        final EditText et = (EditText)findViewById(R.id.searchList);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = et.getText().toString().toLowerCase(Locale.getDefault());
                MainActivity.this.myListAdapter.getFilter().filter(text);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.myList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(memoName[info.position]);
            String[] menuItems = {"Edit" , "Delete"};
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems ={"Edit" , "Delete"};
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = memoName[info.position];

        switch(menuItemName){
            case "Delete":
                memories.delete(memoArray[info.position].getId());
                onResume();
                break;
        }
        Toast.makeText(MainActivity.this , String.format("Selected %s for item %s", menuItemName, listItemName) , Toast.LENGTH_SHORT).show();
        return true;
    }

}
