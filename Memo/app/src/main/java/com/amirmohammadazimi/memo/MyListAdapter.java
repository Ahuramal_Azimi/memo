package com.amirmohammadazimi.memo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amirmohammadazimi.memo.model.Memory;

/**
 * Created by Amir Mohammad Azimi on 4/17/2017.
 */

public class MyListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] memoName;
    private final Memory[] memoArray;
    Boolean listDisplayType;


    public MyListAdapter(Activity context, String[] memoName, Memory[] memoArray, Boolean listDisplayType) {
        super(context, R.layout.mylist, memoName);

        this.context = context;
        this.memoName = memoName;
        this.memoArray = memoArray;
        this.listDisplayType = listDisplayType;
    }

    public View getView(int position, View view, ViewGroup parent) {

//        View rowView;

        if (listDisplayType) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.mylist_extended, null, true);
            }

            TextView txtTitle = (TextView) view.findViewById(R.id.list_title);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            TextView descTxt = (TextView) view.findViewById(R.id.list_desc);
            TextView localTxt = (TextView) view.findViewById(R.id.list_location);
            TextView dateTxt = (TextView) view.findViewById(R.id.list_Date);


            txtTitle.setText(memoName[position]);
            descTxt.setText(memoArray[position].getDescription());
            localTxt.setText(memoArray[position].getLocation());
            dateTxt.setText(memoArray[position].getCalendar().getTime().toString());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(memoArray[position].getImagePath(), options);
            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);

            return view;
        } else {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.mylist, null, true);
            }

            TextView txtTitle = (TextView) view.findViewById(R.id.list_title);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            TextView descTxt = (TextView) view.findViewById(R.id.list_desc);

            txtTitle.setText(memoName[position]);
            descTxt.setText(memoArray[position].getDescription());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(memoArray[position].getImagePath(), options);
            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);

            return view;

        }
    }

}
